# tng-devops tng-k8s experimental branch

This branch is experimental and will be used to deploy SONATA v4.0 in a kuberntes.

## Organization of the repository

* k8s: Folder with the k8s yamls

## Developing

For now we only have k8s yamls ordered in folders

### Prerequisites

* kubernetes cluster 1.11
* kubectl attached to k8s cluster

## Usage

Go to the folder k8s and run the command
```
./deploy-in-k8s.sh
```

## Contributing

To contribute to the development of the tango devops you have to fork the repository, commit new code and create pull requests.

## Licensing

This repository is under Apache 2.0 License

## Lead Developers

* [@felipevicens](https://github.com/felipevicens) Felipe Vicens
